@extends('adminlte/master')

@push('styles')

  <link rel="stylesheet" href="{{asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">

@endpush

@section('content')

<div class="card">
      <div class="card-header">
        <h3 class="card-title">Daftar Pertanyaan</h3>
      </div>
      <!-- /.card-header -->      

      <div class="card-body">

      	@if (session('success'))
		    <div class="alert alert-success" role="alert"> {{session('success')}} </div>
		@endif


      	<a type="button" class="btn btn-primary mb-2" href="/pertanyaan/create">Buat Pertanyaan baru</a>
      	
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No.</th>
            <th>Judul</th>
            <th>Pertanyaan</th>
            <th>Action</th>            
          </tr>
          </thead>
          <tbody>          	
          	@foreach ($daftar_pertanyaan as $key => $pertanyaan)    			
    			<tr>
	          		<td>{{$key+1}}</td>	
	            	<td>{{$pertanyaan->judul}}</td>
	            	<td>{{$pertanyaan->isi}}</td>
	            	<td style="display: flex;">
	            		<a type="button" class="btn btn-info btn-sm" href="/pertanyaan/{{$pertanyaan->id}}">lihat</a>
	            		<a type="button" class="btn btn-warning btn-sm" href="/pertanyaan/{{$pertanyaan->id}}/edit">ubah</a>
	            		<form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
	            			@method('DELETE')
      						@csrf
	            			<button type="submit" class="btn btn-danger btn-sm">Hapus</button>
	            		</form>
	            	</td>            
	          </tr>
			@endforeach
			
          </tbody>
          <!--
          <tfoot>
	          <tr>
	            <th>Rendering engine</th>
	            <th>Browser</th>
	            <th>Platform(s)</th>            
	          </tr>
	      </tfoot>
      	-->
        </table>
      </div>
      <!-- /.card-body -->
    </div>

@endsection

@push('scripts')
  
  <script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });

    //alert("ha ha ha");
  </script>

@endpush