@extends('adminlte/master')

@section('content')

	<div class="card card-primary ml-3 mt-3 mr-3">
      <div class="card-header">
        <h3 class="card-title">Update Pertanyaan id : {{$pertanyaan->id}}</h3>
      </div>
      <!-- /.card-header -->

		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

      <!-- form start -->
      <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
      	@method('PUT')
      	@csrf
        <div class="card-body">
          <div class="form-group">
            <label for="judul_id">Judul</label>
            <input type="text" class="form-control" id="judul_id" placeholder="Masukkan Judul" name="judul" value="{{old('judul',$pertanyaan->judul)}}">
            @error('judul_id')
    			<div class="alert alert-danger">{{ $message }}</div>
			@enderror
          </div>        

          <div class="form-group">
            <label for="pertanyaan_id" >Pertanyaan</label>
            <input type="text" class="form-control" id="pertanyaan_id" placeholder="Masukkan Pertanyaan" name="isi" value="{{old('isi',$pertanyaan->isi)}}" >
            @error('pertanyaan_id')
    			<div class="alert alert-danger">{{ $message }}</div>
			@enderror	
          </div>        
          
          
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Ubah Pertanyaan</button>
        </div>
      </form>
    </div>

@endsection