@extends('adminlte/master')

@section('content')

	<div class="card card-primary ml-3 mt-3 mr-3">
      <div class="card-header">
        <h3 class="card-title">Pertanyaan id : {{$pertanyaan->id}}</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
        
        <div class="card ml-3 mr-3 mt-3 mb-3">
          <div class="card-body" >
            <h5 class="card-title">{{$pertanyaan->judul}}</h5>
            <p class="card-text">{{$pertanyaan->isi}}</p>            
            <a class="btn btn-primary" href="/pertanyaan/{{$pertanyaan->id}}/edit">Ubah Pertanyaan</a>
          </div>
        </div>

      
    </div>

@endsection