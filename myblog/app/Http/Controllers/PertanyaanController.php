<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(Request $Request){

    	$daftar_pertanyaan = DB::table('pertanyaan')->get();

		//dd($daftar_pertanyaan);

        return view('blogs.list', ['daftar_pertanyaan' => $daftar_pertanyaan]);
    	
    }

    public function create(Request $Request){
    	return view('blogs.create');
    }

    public function store(Request $request){

		$validatedData = $request->validate([
        	'judul' => 'required|unique:pertanyaan|max:100',
        	'isi' => 'required'
    	]);

    	$query = DB::table('pertanyaan')->insert([
    		[	
    			'judul' => $request['judul'], 
    			'isi' => $request['isi'],
    			'profil_id'=> 1    			
    		]
    		
		]);

    	return redirect('/pertanyaan')->with('success','Pertanyaan baru dengan judul "'. $request['judul'] .'" berhasil disimpan');	
    }

    public function show($pertanyaan_id){

    	$pertanyaan = DB::table('pertanyaan')
    					->where('id', '=', $pertanyaan_id)
    					->first();
                    	
    	//dd($pertanyaan);

    	return view('blogs.show', ['pertanyaan' => $pertanyaan]);
    }

    public function edit($pertanyaan_id){

    	$pertanyaan = DB::table('pertanyaan')
    					->where('id', '=', $pertanyaan_id)
    					->first();                    	

    	return view('blogs.edit', ['pertanyaan' => $pertanyaan]);
    }

    public function update($pertanyaan_id, Request $request){

    	$validatedData = $request->validate([
        	'judul' => 'required|unique:pertanyaan|max:100',
        	'isi' => 'required'
    	]);

		$query = DB::table('pertanyaan')
              	->where('id', $pertanyaan_id)
              	->update([
              				'judul' => $request['judul'], 
    						'isi' => $request['isi']
          				]);

    	return redirect('/pertanyaan')->with('success','Pertanyaan dengan judul "'. $request['judul'] .'" berhasil diubah');	
    	
    }

    public function destroy($pertanyaan_id){

    	$pertanyaan = DB::table('pertanyaan')
    					->where('id', '=', $pertanyaan_id)
    					->first();
    	$query = DB::table('pertanyaan')
              	->where('id', $pertanyaan_id)
              	->delete();					

    	return redirect('/pertanyaan')->with('success','Pertanyaan dengan judul "'. $pertanyaan->judul .'" berhasil dihapus');	
    }
}
